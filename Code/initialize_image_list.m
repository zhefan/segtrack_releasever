function initialize_image_list(file_name, file_prefix, start_frame, end_frame)
%function initialize_image_list(file_name, file_prefix, start_frame, end_frame)

% file_name = 'egocentric';
% file_prefix = 'Yin_American_';
% start_frame = 1701;
% end_frame = 1789;


fileID = fopen(sprintf('../ImageSets/%s.txt',file_name),'w');
fprintf(fileID, '%s/\n', file_name);
for i = start_frame:end_frame
    fprintf(fileID, '%s%d\n', file_prefix, i);
end

fclose(fileID);


