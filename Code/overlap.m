function overlappingScore = overlap(gt, pred)
% gt = gtMask;
% pred = tempSeg;

temp = gt .* pred;
intersection = numel(find(temp == 1));

temp = gt + pred;
union = numel(find(temp ~= 0));

overlappingScore = intersection / (union + eps);

end
