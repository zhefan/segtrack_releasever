function run_segtrack(start_frame, end_frame, file_prefix)

% close all
% clear all
% clc

file_name = 'egocentric';
% start_frame = 2857;
% end_frame = 2879;

add_all_paths
SegTrack_config
%% clean up
system(sprintf('rm *.jpg'))
system(sprintf('rm *.tmp'))
system(sprintf('rm -rf ../MyMeasurements'))
tic
initialize_image_list(file_name, file_prefix, start_frame, end_frame);
segtrack_run_all('../ImageSets/egocentric.txt', '../')
toc