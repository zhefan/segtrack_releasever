#include <unordered_set>
#include <vector>
#include "mex.h"

using namespace std;

// Input is a matrix with width*height rows and nsegms columns, so that we can take each column as a bitset and hash it.
// 2 outputs, masks, the bitset specifying a superpixel, and map, a mapping from each superpixel to the bitset
// This is not fastest yet, since it's doing a copy construction to bitset, best should be doing this in-place
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    bool *x;
    int nsegms, wh;
    unordered_set<vector<bool> > hash_map(1000);
    double *idx;
    pair<iterator,bool> insert_result;
    if (nrhs < 1 || mxGetClassID(prhs[0]) != mxLOGICAL_CLASS)
    {
        mexPrintf("assign_superpix takes only 1 boolean matrix!");
        return;
    }
    x = (bool *) mxGetData(prhs[0]);
    wh = mxGetM(prhs[0]);
    nsegms = mxGetN(prhs[0]);

    if (nlhs > 1)
    {
        plhs[1] = mxCreateDoubleMatrix(wh,1,mxREAL);
        idx = mxGetPr(plhs[1]);
    }

    for(int i=0;i<wh;i++)
    {
        vector<bool> bs(&x[i*nsegms], &x[(i+1)*nsegms]);
        insert_result = hash_map.insert(bs);
        idx[i] = (double) (insert_result.first - bs.cbegin()) / sizeof(bs.cbegin());
    }
    for (int i=0;i<hash_map.bucket_count();i++)
    {
        for (auto local_it = hash_map.begin(i)
    }
}