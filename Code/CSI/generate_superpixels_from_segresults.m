function [sup_size, sup_assignment, scores, bag_ids, Pixels] = generate_superpixels_from_segresults(masks, scores, bag_ids, score_cutoff, size_cutoff, visualize)
% For one image, generate superpixels from segmentation results.
% Output the results by returning argument, no savefiles.
    if ~exist('size_cutoff','var')
        size_cutoff = 50;
    end
    if ~exist('visualize','var')
        visualize = false;
    end
    if ~exist('bag_ids','var') || isempty(bag_ids)
        bag_ids = (1:size(scores,1))';
    end
    max_scores = max(scores,[],2);
    if visualize
        cmap = SvmSegm_labelcolormap(255);
    end
        to_keep = max_scores > score_cutoff;
        masks = masks(:,:,to_keep);
        bag_ids = bag_ids(to_keep);
        scores = scores(to_keep,:);
        sz = size(masks);
        if length(sz)==2
            sz(3) = 1;
        end
        [masks2,~,idx2] = unique(reshape(masks,sz(1)*sz(2),sz(3)), 'rows');
        % No segments
        if isempty(masks2)
            Pixels = [];
            sup_assignment = [];
            sup_size = [];
            return;
        end
        hs = histc(idx2,1:size(masks2,1));
        [num_pixels, sort_idx] = sort(hs,'descend');
        [small_assign,simple_ones] = assign_small_patches(masks2(sort_idx,:), num_pixels >= size_cutoff);
        sup_assignment = masks2(sort_idx(simple_ones),:);
        sup_size = num_pixels(simple_ones);
%        sort_idx = sort_idx(1:select);
        Pixels = zeros(size(masks,1),size(masks,2));
        % re-sort it to make the index nicer
        for j=1:max(small_assign)
            Pixels(ismember(idx2,sort_idx(small_assign==j))) = j;
        end
        if visualize
            imshow(Pixels,cmap);
        end
end

function [small_assign,simple_ones] = assign_small_patches(masks, to_keep)
    small_assign = zeros(length(to_keep),1);
    simple_ones = find(to_keep);
    small_assign(simple_ones) = 1:length(simple_ones);
    s2 = find(~to_keep);
    if ~isempty(s2)
        idx = knnsearch(double(masks(simple_ones,:)),double(masks(s2,:)));
        small_assign(s2) = idx;
    end
end